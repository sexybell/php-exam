<?php

namespace Tests\Feature;

use App\Models\Posts;
use App\Models\User;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Support\Str;
use Tests\TestCase;

class PostControllerTest extends TestCase
{
    /**
     * A basic feature test example.
     *
     * @return void
     */

    public function testGetList()
    {
        $user = User::factory()->create();
        $token = $user->createToken('testToken')->plainTextToken;
        $response = $this->withHeader('Authorization', 'Bearer ' . $token)
            ->json('GET', '/api/list/');
        $response->assertStatus(200);
    }

    public function testCreatePost()
    {
        $user = User::factory()->create();
        $token = $user->createToken('testToken')->plainTextToken;
        $postData = [
            'title' => 'Test Post',
            'description' => 'This is a test post',
            'content' => 'Lorem ipsum dolor sit amet',
        ];

        $response = $this->withHeader('Authorization', 'Bearer ' . $token)
            ->json('POST', '/api/create', $postData);

        $response->assertStatus(201)->assertJson(['data' => [
            'title' => 'Test Post',
            'description' => 'This is a test post',
            'content' => 'Lorem ipsum dolor sit amet',
        ]]);
    }

    public function testUpdatePost()
    {
        $user = User::factory()->create();
        $token = $user->createToken('testToken')->plainTextToken;

        $post = Posts::query()->whereNull('deleted_at')->first()->only(['title', 'content', 'description', 'id']);
        $post['title'] = 'Update Title ' . Str::random(10);
        $post['slug'] = $post['title'];
        $id = $post['id'];
        $response = $this->withHeader('Authorization', 'Bearer ' . $token)
            ->json('PUT', '/api/update/'. $id, $post);
        $response->assertStatus(201);
    }

    public function testDetail()
    {
        $user = User::factory()->create();
        $token = $user->createToken('testToken')->plainTextToken;

        $post = Posts::query()->whereNull('deleted_at')->first()->toArray();
        $id = $post['id'];
        $response = $this->withHeader('Authorization', 'Bearer ' . $token)
            ->json('GET', '/api/detail/'. $id);
        $response->assertStatus(200);
    }

    public function testDelete()
    {
        $user = User::factory()->create();
        $token = $user->createToken('testToken')->plainTextToken;

        $post = Posts::query()->whereNull('deleted_at')->first()->toArray();
        $id = $post['id'];
        $response = $this->withHeader('Authorization', 'Bearer ' . $token)
            ->json('DELETE', '/api/delete/'. $id);
        $response->assertStatus(200);
    }
}
