<?php

namespace App\Http\Controllers;

use App\Http\Requests\CreatePostRequest;
use App\Services\PostService;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Auth;

class PostController extends Controller
{
    protected PostService $postService;

    public function __construct(PostService $postService)
    {
        $this->postService = $postService;
    }

    /**
     * Get list of post
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function list() :object
    {
        try {
            $data = $this->postService->list();
            return response()->json(['data' => $data, 'msg' => 'Action completed successfully.']);
        } catch (\Exception $e) {
            return response()->json(['data' => [], 'msg' => $e->getMessage()]);
        }
    }

    /**
     *
     * Get post detail
     * @param $id
     * @return \Illuminate\Http\JsonResponse
     */
    public function detail($id) :object
    {
        try {
            $detail = $this->postService->detail($id);
            if ($detail) {
                return response()->json(['data' => $detail, 'msg' => 'Action completed successfully.']);
            }
            return response()->json(['data' => [], 'msg' => 'Post Not Found']);
        } catch (\Exception $exception) {
            return response()->json(['data' => [], 'msg' => $exception->getMessage()], 202);
        }
    }

    /**
     * Create a new post
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function create(Request $request) :object
    {
        try {
            $validate = new CreatePostRequest();
            $validator = Validator::make($request->all(), $validate->rules(), $validate->messages());
            if ($validator->fails()) {
                return response()->json(['errors' => $validator->errors(), 'status' => 422]);
            }
            $userId = Auth::id();
            $params = $request->all();

            //Create slug from title
            $params['slug'] = $this->slugConvert($params['title']);

            $params['user_id'] = $userId;
            $create = $this->postService->create($params);
            if ($create) {
                return response()->json(['data' => $create, 'msg' => 'Created'], 201);
            }
            return response()->json(['data' => $create, 'msg' => 'An error occurred while processing your request.']);
        } catch (\Exception $exception) {
            return response()->json(['data' => [], 'msg' => $exception->getMessage()]);
        }
    }

    /**
     *
     * Update a exist post
     * @param Request $request
     * @param $id
     * @return \Illuminate\Http\JsonResponse
     */
    public function update(Request $request, $id) :object
    {
        try {
            $validate = new CreatePostRequest();
            $validator = Validator::make($request->all(), $validate->rules(), $validate->messages());
            if ($validator->fails()) {
                return response()->json(['errors' => $validator->errors()], 422);
            }
            $isExistPost = $this->postService->detail($id);
            if ($isExistPost) {
                $params = $request->all();

                //Create slug from title
                $params['slug'] = $this->slugConvert($params['title']);

                $update = $this->postService->update($id, $params);
                if ($update) {
                    return response()->json(['data' => $update, 'msg' => 'Updated'], 201);
                }
                return response()->json([
                    'data' => $update,
                    'msg' => 'An error occurred while processing your request.'
                ]);
            }
            return response()->json(['data' => [], 'msg' => 'Post not found']);

        } catch (\Exception $exception) {
            return response()->json(['data' => [], 'msg' => $exception->getMessage()], 202);
        }
    }

    /**
     * Delete a exist post
     * @param $id
     * @return \Illuminate\Http\JsonResponse
     */
    public function delete($id) :object
    {
        try {
            $isExistPost = $this->postService->detail($id);
            if ($isExistPost) {
                $delete = $this->postService->delete($id);
                return response()->json(['data' => $delete, 'msg' => 'Deleted']);
            }
            return response()->json(['data' => [], 'msg' => 'Post not found'], 202);
        } catch (\Exception $exception) {
            return response()->json(['data' => [], 'msg' => $exception->getMessage()], 202);
        }
    }
}
