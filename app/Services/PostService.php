<?php

namespace App\Services;

use App\Repositories\PostRepository;

class PostService
{
    protected PostRepository $postRepository;

    public function __construct(PostRepository $postRepository)
    {
        $this->postRepository = $postRepository;
    }

    /**
     * @return \Illuminate\Database\Eloquent\Builder[]|\Illuminate\Database\Eloquent\Collection
     */
    public function list() :object
    {
        return $this->postRepository->list();
    }


    public function detail($id) :object
    {
        return $this->postRepository->detail($id);
    }

    public function create($params) :object
    {
        return $this->postRepository->create($params);
    }

    public function update($id,$params) :bool
    {
        return $this->postRepository->update($id,$params);
    }

    public function delete($id) :bool
    {
        return $this->postRepository->delete($id);
    }
}
