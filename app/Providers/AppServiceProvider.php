<?php

namespace App\Providers;

use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\ServiceProvider;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }

    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        Validator::extend('check_unique', function ($attribute, $value, $parameters, $validator) {
            $postData = $validator->getData();
            $table = $parameters['0'];
            $field = $parameters['1'];

            $id = null;
            if (isset($postData['id'])) {
                $id = $postData['id'];
            }
            $data = DB::table($table)
                ->where([
                    $table . '.' . $field => $value,
                ])->whereNull('deleted_at');
            if ($id) {
                $data = $data->where($table . '.id', '!=', $id);
            }
            $data = $data->first();
            if ($data) {
                return false;
            }
            return true;
        });
    }
}
