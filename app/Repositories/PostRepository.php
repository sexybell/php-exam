<?php

namespace App\Repositories;

use App\Models\Posts;

class PostRepository
{
    public Posts $posts;

    public function __construct(Posts $posts)
    {
        $this->posts = $posts;
    }

    public function list() :object
    {
        $query = $this->posts->query()->whereNull('deleted_at');
        return $query->get();
    }

    public function detail($id) :object
    {
        return $this->posts->query()->whereNull('deleted_at')->where('id', $id)->first();
    }

    public function create($params) :object
    {
        return $this->posts->query()->create($params);
    }

    public function update($id,$params) :bool
    {
        return $this->posts->query()->where('id', $id)->update($params);
    }

    public function delete($id) :bool
    {
        return $this->posts->query()->where('id', $id)->update(['deleted_at' => now()]);
    }


}
