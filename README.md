# Blog APIs

This is API for a blog page.

## Setup

### Prerequisites

- PHP >= 7.4
- Composer
- MySQL
- Node.js & NPM (for frontend assets)

### Installation

1. Install dependencies:
   ```bash
   composer install
2. Copy the .env.example file to .env
   ```bash
   cp .env.example .env
3. Generate application key
   ```bash
   php artisan key:generate
4. Configure the database in the .env file:
    ```bash
    DB_CONNECTION=mysql
    DB_HOST=127.0.0.1
    DB_PORT=3306
    DB_DATABASE=your_database_name
    DB_USERNAME=your_database_username
    DB_PASSWORD=your_database_password
5. Migrate and seeding the database
    ```bash
   php artisan migrate --seed
6. Serve the application:
    ```bash
   php artisan serve

### API Document

1. Authentication.

- **URL:** `/api/login`
- **Method:** `POST`
- **Params:**
    - `email` (required, string): Email for login.
    - `password` (required, string): Password.

**Example Request:**
```http
POST /api/login

{
  "email": "admin@gmail.com",
  "password": "12345678"
}
```
**Example Response:**
```http
{
    "token": "39|7JVnEOpoO2XAHp3UOuZlbEnAZxHfqsECTYvAK0lx3a4c71f6"
}
```
2. Create a new post

- **URL:** `/api/post/create`
- **Method:** `POST`
- **Params:**
    - `title` (required): Title of the post.
    - `description`: Description of the post.
    - `content` (required): Content of the post.

**Example Request:**
```http
POST /api/post/create
Content-Type: application/json
Authorization: Bearer YOUR_TOKEN_HERE

{
  "title": "New Post",
  "description": "This is a new description",
  "content": "This is a new content",
}
```
**Example Response:**
```http
{
    "data": {
        "title": "New Post",
        "description": "This is a new description",
        "content": "This is a new content",
        "slug": "new-post-0TDg2yx18y",
        "user_id": 1,
        "updated_at": "2024-04-20T16:42:55.000000Z",
        "created_at": "2024-04-20T16:42:55.000000Z",
        "id": 1
    },
    "msg": "Created"
}
```
3. Update a exist post

- **URL:** `/api/post/update/{id}`
- **Method:** `PUT`
- **Params:**
    - `title` (required): Title of the post.
    - `description`: Description of the post.
    - `content` (required): Content of the post.

**Example Request:**
```http
POST /api/post/update
Content-Type: application/json
Authorization: Bearer YOUR_TOKEN_HERE

{
  "title": "New post title",
  "description": "This is a new description",
  "content": "This is a new post content",
}
```

**Example Response:**
```http
{
    "data": 1, //true
    "msg": "Updated"
}
```
4. Get list of posts

- **URL:** `/api/post/list`
- **Method:** `GET`

**Example Request:**
```http
GET /api/post/list
Content-Type: application/json
```
**Example Response:**
```http
{
    "data": [
        {
            "id": 1,
            "title": "Ut amet neque fugit reiciendis recusandae.",
            "description": "Quaerat non enim et nihil. Dolorum pariatur eius facilis occaecati animi rerum neque incidunt. Voluptatum dolor non blanditiis. Rem dolor at dolorum et molestias.",
            "content": "Omnis architecto veritatis repellat et qui eveniet eveniet. Quas neque optio odio ut ut. Ut quo voluptas voluptatibus ipsum. Nobis animi nihil ratione delectus voluptatem est.",
            "slug": "hic-occaecati-voluptas-perspiciatis-ex",
            "user_id": 1,
            "deleted_at": null,
            "created_at": "2024-04-20T15:45:45.000000Z",
            "updated_at": "2024-04-20T15:45:45.000000Z"
        },
        {
            "id": 2,
            "title": "Et et ex blanditiis quia laudantium veritatis et molestiae.",
            "description": "Non odio beatae et necessitatibus voluptate. Quia aliquid omnis ea repellat ab. Pariatur nihil corrupti dolor aut. Recusandae occaecati vel sed vero.",
            "content": "Velit itaque ratione laborum officiis. Cum at laudantium veniam assumenda sed. Ullam occaecati maxime qui quia sed dolores fuga.",
            "slug": "fugit-ea-autem-dolor",
            "user_id": 1,
            "deleted_at": null,
            "created_at": "2024-04-20T15:45:45.000000Z",
            "updated_at": "2024-04-20T15:45:45.000000Z"
        }
    ],
    "msg": "Successfully"
}
```

5. Detail of a post

- **URL:** `/api/post/detail/{id}`
- **Method:** `GET`

**Example Response:**
```http
{
    "data": {
        "id": 32,
        "title": "32",
        "description": "Mô tả bài viết đã cập nhật 1",
        "content": "Nội dung bài viết đã cập nhật",
        "slug": "32-LiHDtPdcAu",
        "user_id": 4,
        "deleted_at": null,
        "created_at": "2024-04-20T18:44:45.000000Z",
        "updated_at": "2024-04-21T02:37:22.000000Z"
    },
    "msg": ""
}
```
6. Delete a post

- **URL:** `/api/post/delete/{id}`
- **Method:** `DELETE`

**Example Request:**
```http
DELETE /api/post/delete/{id}
Content-Type: application/json
Authorization: Bearer YOUR_TOKEN_HERE
```
**Example Response:**
```http
{
    "data": 1, //true
    "msg": "Deleted"
}
```
